(function (module) {
	"use strict";
	
    var requests = function ($http, $q) {
        
		var apiEndpoint = "https://jsonp.afeld.me/?url=http://yugiohprices.com";
			
		var getCardInfo = function (cardName) {
			var endpoint = apiEndpoint + "/api/card_data/" + cardName;// + "&callback=JSON_CALLBACK";
			return $http.get(endpoint)
			.then(
				function(response) {
					return response.data.data;
				},
				function(error){
					return error;
				}
			);
		};
		
		return {
			getCardInfo: getCardInfo
		};
    }

    module.factory("requests", ["$http", "$q", requests]);

}(angular.module("app")))