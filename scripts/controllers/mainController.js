(function (module) {
	"use strict";
	
    var mainController = function ($scope, $document, $interval, requests) {
        
        $scope.title = "Yu-Gi-Oh! Deck Browser";
		$scope.tableHeight = !$(document) ? "500" : $(document).height();

		$scope.loadingData = true;
		$scope.viewInfo = false;
		
		$scope.cards = [];
		$scope.cardToShow = {};
        $scope.deck = [
			{
				name: "Burial from a Different Dimension",
			},
			{
				name: "Charge of the Light Brigade",
			},
			{
				name: "Infernoid Antra",
			},
			{
				name: "Infernoid Attondel",
			},
			{
				name: "Infernoid Decatron",
			},
			{
				name: "Infernoid Devyaty",
			},
			{
				name: "Infernoid Harmadik",
			},
			{
				name: "Infernoid Onuncu",
			},
			{
				name: "Infernoid Patrulea",
			},
			{
				name: "Infernoid Pirmais",
			},
			{
				name: "Infernoid Seitsemas",
			},
			{
				name: "Lyla, Lightsworn Sorceress",
			},
			{
				name: "Monster Gate",
			},
			{
				name: "One for One",
			},
			{
				name: "Raiden, Hand of the Lightsworn",
			},
			{
				name: "Reasoning",
			},
			{
				name: "Time-Space Trap Hole",
			},
			{
				name: "Torrential Tribute",
			},
			{
				name: "Upstart Goblin",
			},
			{
				name: "Void Seer",
			}
		];
		
		$scope.countdown = 0;
		
        var startRequest = $interval(function () {
            requests.getCardInfo($scope.deck[$scope.countdown].name)
            .then(function (data){
                $scope.cards.push(data);
				$scope.loadingData = false;
            },
			function(error){
				console.log(error);
			});
			
            $scope.countdown++;
			
        }, 300, $scope.deck.length);
		
		$scope.showCardInfo = function(cardIndex){
			$scope.viewInfo = true;
			$scope.cardToShow = $scope.cards[cardIndex];
			$scope.cardToShow.imageUrl = "http://yugiohprices.com/api/card_image/" + $scope.cardToShow.name;
		};
		
		$scope.checkAttribute = function(attribute) {
			if (attribute != null && attribute >= 0 ) {
				return true;
			} else {
				return false;
			}
		};
		
    }
	
    module.controller("mainController", ["$scope", "$document", "$interval", "requests", mainController]);

}(angular.module("app")))