I. File list
------------
index.html				- main application page
content/main.css			- main custom css file
images/Favicon.ico			- application page icon
images/loading.gif			- spinner image
bower.json				- file that contains all the project dependencies with other libraries (angular, jquery, bootstrap)
package.json				- file that contains all project dependencies to run the project and the tests
karma.conf.js				- configuration file to init and bootstrap karma for tests
bower_components			- folder that contains library project dependencies
node_modules				- folder that contains all project dependencies
scripts/app.js				- main angular module
scripts/services/requests.js		- api requests
scripts/controllers/mainController.js	- main cotroller
README.txt				- this file

II. Build and run
----------------
 1. prerequisites nodejs

 from project's folder type:
 2. npm install
 3. bower install
 4. start project "npm start" use 127.0.0.1:8080 url on a browser
 5. run test "npm test"

III. Description
-------------------
1. Application's functionality was ok to be inserted inside one controller
2. I created a service using factory to separate http calls from view and inject it to the controller
3. I had to retrieve the type of each card, that demanded to make all the api calls at once and cached all the data retrieved into a private controller property
4. I used an interval between api calls because an error was occuring (too many requests 429)
5. Image is retrieved uppon click
6. Used karma and jasmine for testing
7. Load controller and test the title of the page