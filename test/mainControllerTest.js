describe('TestMainController', function () {


  beforeEach(module('app'));
	var controller, $scope;
  beforeEach(inject(function ($controller, $rootScope) {
    $scope = $rootScope.$new();
    controller = $controller('mainController', {
      $scope: $scope
    });
  }));

  it('Application page title', function () {
    expect($scope.title).toBe("Yu-Gi-Oh! Deck Browser");
  });
});